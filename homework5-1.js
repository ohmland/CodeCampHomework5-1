let fs = require('fs');

let arrTemp = fs.readFileSync('homework1-4.json','utf8',(err, data)=>{
    if (err)
        console.error(err);
    else
        return data;
});

let arr = JSON.parse(arrTemp);
let originalFileLength = arr.length;
exports.originalFileLength = originalFileLength;

let countEyeColor = eyeColor(arr);
exports.countEyeColor = countEyeColor;

let countGender = gender(arr);
exports.countGender = countGender;

let countFriends = friendsCount(arr);
exports.countFriends = countFriends;

//---------------Read File-----------------------------
function readData(){
    return new Promise(function(resolve, reject) {
        fs.readFile('homework1-4.json','utf8',(err, data)=>{
            if (err)
                reject(err);
            else
                resolve(data);
        });
    })
}
exports.readData = readData;

//---------------write File-----------------------------
function writeData(fileName ,content){
    return new Promise(function(resolve,reject){
        fs.writeFile(fileName,JSON.stringify(content),'utf8',(err)=>{
            if (err)
                reject(err);
            else
                resolve();
                console.log("Write file " +fileName+ " success!");
            //fs.writeFile('homework5-1_eyes.json',JSON.stringify(countEyeColor),'utf8',(err, data)=>{});
        });
    })
}
exports.writeData = writeData;

//---------------Eye Color-----------------------------
function eyeColor(arr){
    let eyeColorTemp = [];
    let countEyeColor = {};
    let result = {};
    for (let i = 0; i < arr.length; i++) {
        for (let item in arr[i]) {
            if(item === "eyeColor")
            {
                eyeColorTemp.push(arr[i].eyeColor);
            }
        }
    }

    for (i in eyeColorTemp) {
        result[eyeColorTemp[i]] = 0;
    }

    let count = 0;
    for (let i in result) {
        for (let item in eyeColorTemp) {
            if (i===eyeColorTemp[item]) {
                count++;
            }
        }
        countEyeColor[i] = count;
        count = 0;
    }
    
    return countEyeColor;
}

//---------------Gender-----------------------------
function gender(arr){
    let genderTemp = [];
    let countGender = {};
    let GenderResult = {};
    for (let i = 0; i < arr.length; i++) {
        for (let item in arr[i]) {
            if(item === "gender")
            {
                genderTemp.push(arr[i].gender);
            }
        }
    }

    for (i in genderTemp) {
        GenderResult[genderTemp[i]] = 0;
    }

    let count = 0;
    for (let i in GenderResult) {
        for (let item in genderTemp) {
            if (i===genderTemp[item]) {
                count++;
            }
        }
        countGender[i] = count;
        count = 0;
    }
    return countGender;
}

//---------------Friends-----------------------------
function friendsCount(arr){
    let arrAns = [];
    
    for (let i = 0; i < arr.length; i++)
    {
        let obj = {};
        for (let item in arr[i])
        {
            if (item === '_id')
            {
                obj[item] = arr[i][item];
            }else if(item === 'friends')
            {
                obj[item] = arr[i][item].length;
            }
        }
        arrAns[i] = obj;
    }
    return arrAns;
}

//---------------Seperate File-----------------------------
async function seperateFile(file, key, callback){
    try{
        let dataFromReadFile = await readFile;
        async 
    }
    catch(error){

    }
}