let fs = require('fs');
let assert = require('assert');
let homework = require('./homework5-1.js');
let readData = homework.readData;
let writeData = homework.writeData;
let countEyeColor = homework.countEyeColor;
let countGender = homework.countGender;
let countFriends = homework.countFriends;
let originalFileLength = homework.originalFileLength;


seperate();

async function seperate(){
    await writeData('homework5-1_eyes.json', countEyeColor);
    await writeData('homework5-1_gender.json', countGender);
    await writeData('homework5-1_friends.json', countFriends);
}

describe('tdd lab', function() {
    describe('#checkFileExist()', function() {
        it('should have homework5-1_eyes.json existed', function() {
            assert.strictEqual(fs.existsSync('homework5-1_eyes.json'),true,'file not found.');
        });
        it('should have homework5-1_gender.json existed', function() {
            assert.strictEqual(fs.existsSync('homework5-1_gender.json'),true,'file not found.');
        });
        it('should have homework5-1_friends.json existed', function() {
            assert.strictEqual(fs.existsSync('homework5-1_friends.json'),true,'file not found.');
        });
    });

    describe('#objectKey()', function() {
         it('should have same object key stucture as homework5-1_eyes.json', function() {
            let dataFromJson = fs.readFileSync('homework5-1_eyes.json','utf8',(err, data)=>{
                if (err)
                    console.error(err);
                else
                    return data;
            });
            let dataAfterParse = JSON.parse(dataFromJson);
            let expectValue = {"brown":11,"blue":6,"green":6};
            assert.deepEqual(Object.keys(dataAfterParse), Object.keys(expectValue),'Object key structure not a same');
         });
         it('should have same object key stucture as homework5-1_friends.json', function() {
            let dataFromJson = fs.readFileSync('homework5-1_gender.json','utf8',(err, data)=>{
                if (err)
                    console.error(err);
                else
                    return data;
            }); 
            let dataAfterParse = JSON.parse(dataFromJson);
            let expectValue = {"female":0,"male":0};
            assert.deepEqual(Object.keys(dataAfterParse), Object.keys(expectValue),'Object key structure not a same');
         });
         it('should have same object key stucture as homework5-1_friends.json', function() {
            let dataFromJson = fs.readFileSync('homework5-1_friends.json','utf8',(err, data)=>{
                if (err)
                    console.error(err);
                else
                    return data;
            });
            let dataAfterParse = JSON.parse(dataFromJson);
            let expectValue = [{"_id":"5a3711070776d02ed87d2100","friends":3},{"_id":"5a371107493db1e1fd7a153f","friends":1},{"_id":"5a3711079f44c94b32f42b0b","friends":3},{"_id":"5a3711071f8377f04dcc0647","friends":0},{"_id":"5a3711072e74ea6f409fdb43","friends":2},{"_id":"5a37110722a47e68e57f142f","friends":3},{"_id":"5a371107066b63e31205f928","friends":1},{"_id":"5a371107d29f5b88763ed067","friends":3},{"_id":"5a371107e7cb282674fa91ba","friends":0},{"_id":"5a37110765fb042dc2916d9d","friends":1},{"_id":"5a371107e92ac1efaa56a45b","friends":3},{"_id":"5a3711076224800cc138e3c5","friends":1},{"_id":"5a371107cbb692e1ccd27ded","friends":1},{"_id":"5a37110708e52d671991d276","friends":0},{"_id":"5a371107208b81a965d234bd","friends":1},{"_id":"5a37110767113b570288b660","friends":2},{"_id":"5a37110753339359bb11244f","friends":1},{"_id":"5a3711072da924792a4b5cdf","friends":0},{"_id":"5a37110791bdc1ee45753892","friends":0},{"_id":"5a3711075fb9f745fc1eea7f","friends":3},{"_id":"5a371107b8dedc7ce188485a","friends":3},{"_id":"5a371107de6e9c5992f616db","friends":2},{"_id":"5a3711076adf9da3c64e4beb","friends":2}];
            assert.deepEqual(Object.keys(dataAfterParse), Object.keys(expectValue),'Object key structure not a same');
         });
    });
    
    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', function() {
            let dataFromJson2 = fs.readFileSync('homework5-1_friends.json','utf8',(err, data)=>{
                if (err)
                    console.error(err);
                else
                    return data;
            });
            let dataAfterParse2 = JSON.parse(dataFromJson2);
            assert.equal(dataAfterParse2.length , originalFileLength,'Size not equal original file');
         });
    });
    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', function() {
            let sumParameter = 0;
            let keyArray = Object.keys(countEyeColor);
            keyArray.forEach(element => {
                sumParameter = sumParameter + countEyeColor[element];
            });
            
            assert.equal(sumParameter, 23, 'Sum of eyes not equal');
        });
    });

    describe('#sumOfGender()', function() {
        it('should have sum of gender as 23', function() {
            let sumParameter = 0;
            let keyArray = Object.keys(countGender);
            keyArray.forEach(element => {
                sumParameter = sumParameter + countGender[element];
            });

            assert.equal(sumParameter, 23, 'Sum of gender not equal');
        });
    });
});